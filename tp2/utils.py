import numpy as np
import sympy as sp

# TODO: set as False for delivery!!!
DEBUG = True

""" [BEGIN] -- Global variables """
INFEASIBLE = 0
UNBOUNDED = 1
OPTIMAL = 2

CONCLUSION_FILE = "conclusao.txt"
ITERATIONS_FILE = "simplex_iterations.txt"

PRINT_FORMAT_CERTIFICATE = '%.6f'
PRECISION_DECIMALS_CERTIFICATE = 6

PRINT_FORMAT_SOLUTION = '%.5f'
PRECISION_DECIMALS_SOLUTION = 5

PRINT_FORMAT_TARGET_VALUE = '%.5f'

DEBUG_TOLERANCE = 0.000001
""" [END] -- Global variables """

""" [BEGIN] -- Debug functions """
def debug(arg):
    if DEBUG:
        print("\n\n[DEBUG] "+ str(arg) +" [DEBUG]")

def debug_matrix(matrix):
    if DEBUG:
        # The printing results will wrap the line after 300 characters
        # suppress=True means that no scientific notation is allowed
        # All the elements will be simplified into Rationals (fractions), where the denominator is not greater then 10^6
        np.set_printoptions(
            linewidth=300,
            suppress=True,
            formatter={
                'all':lambda x: str(sp.nsimplify(x, rational=True, tolerance=DEBUG_TOLERANCE))
                }
            )
        print("---[DEBUG_MATRIX]---")
        print(matrix)
        print("---[DEBUG_MATRIX]---\n")

def debug_dict(data):
    if DEBUG:
        print("---[DEBUG_DICT]---")
        for key, value in data.items():
            print("\t"+str(key) +" is:")
            print(value)
        print("---[DEBUG_DICT]---\n")

def debug_list_of_tuples(list):
    if DEBUG:
        print("---[DEBUG_TUPLES]---")
        print("---ratio, line, column---")
        for tuple in list:
            print(tuple)
        print("---[DEBUG_TUPLES]---\n")
""" [END] -- Debug functions """

""" [BEGIN] -- Matrix dimension functions """
def columns(matrix):
    return dimensions(matrix)[1]

def lines(matrix):
    return dimensions(matrix)[0]

# If the matrix has only one column, a Numpy array will throw an exception
def dimensions(matrix):
    try:
        d = (matrix.shape[0], matrix.shape[1])
    except IndexError:
        d = (1, matrix.shape[0])
    return d
""" [END] -- Matrix dimension functions """

""" [BEGIN] -- Matrix creation functions """
# Given a number of lines, creates a matrix in the format:
# 0 0 ... 0
# 1 0 ... 0
# 0 1 ... 0
# 0 0 ... 1
# Which will be named a I_certificate matrix
def create_I_certificate_matrix(lines):
    identity_matrix = np.eye(lines, dtype=sp.Rational)
    certificate_vector = np.zeros((1, lines), dtype=sp.Rational)
    I_certificate_matrix = np.concatenate((certificate_vector, identity_matrix), axis=0)
    return I_certificate_matrix

# Given a matrix in standart_format, creates the tableau matrix:
# 0 ... 0 | -c | 0
# I matrix| A  | B
def create_tableau_matrix_from_standart_format(standart_format_matrix, A_lines):
    # Multiply the C vector by -1 to put in the tableau
    standart_format_matrix[0,:-A_lines-1] *= -1

    # Create a I_certificate to be inserted in the left side of tableau, for registring operations and for the certificate
    left_matrix = create_I_certificate_matrix(A_lines)
    tableau_matrix = np.concatenate((left_matrix, standart_format_matrix), axis=1)
    return tableau_matrix
""" [END] -- Matrix creation functions """

""" [BEGIN] -- Tableau_Matrix constituents functions """
## certificate     | C from tableau | optimal_value ##
## Identity_matrix | A              | B             ##
def B_vector(matrix):
    return matrix[1:,-1:]

def C_vector_from_tableau(matrix, certificate_items = 0):
    first_c_item = certificate_items
    last_c_item = columns(matrix)-1
    return matrix[0,first_c_item:last_c_item]

def get_target_value(matrix):
    return matrix[0,-1]

def get_certificate_from_tableau(matrix, certificate_items):
    certificate = np.array([], dtype=sp.Rational)
    certificate = matrix[0, 0:certificate_items]
    return certificate
""" [END] -- Tableau_Matrix constituents functions """

""" [BEGIN] -- Solution functions """
def check_if_column_basic(matrix, column):
    num_zeros = 0
    num_ones = 0
    one_value_index = -1

    for i in range(0, lines(matrix)):
        if (matrix[i, column] == 0):
            num_zeros += 1
        elif (matrix[i, column] == 1):
            # It is not a Xb if the 1 value corresponds to a negative number in B
            temp_value = sp.nsimplify(matrix[i, -1:], rational=True, tolerance=DEBUG_TOLERANCE)
            if (temp_value < 0):
                return (False, -1)
            num_ones += 1
            one_value_index = i
            # It is not a Xb if the column has other number other than 0 or 1
        else:
            return (False, -1)

    # It is Xb if the column has exactly one 1 and the rest of it are 0s
    if (num_zeros == lines(matrix)-1) and (num_ones == 1):
        return (True, one_value_index)

    return (False, -1)

def find_basic_columns(matrix, certificate_items = 0):
    solution = np.array([], dtype=sp.Rational)
    possible_columns_base = []
    possible_lines = []


    # Iterates through the columns in the A matrix (if there is no certificate matrix, the first column is used)
    first_column = certificate_items
    for curr_column in range(first_column, columns(matrix) - 1):
        is_basic_column, solution_line_index = check_if_column_basic(matrix, curr_column)

        # If the column is basic, appends the value into the solution array and
        # the column index into the possible_columns_base array
        if is_basic_column:
            # If it exists another basic column which has the same line/column relation, it is not added to the final base
            # It means the columns base chosen is always the first seen
            if solution_line_index in possible_lines:
                solution = np.append(solution, 0)
            else:
                possible_columns_base.append(curr_column)
                possible_lines.append(solution_line_index)
                solution = np.append(solution, matrix[solution_line_index, -1:])
        # Else, the corresponding variable is zero for that column
        else:
            solution = np.append(solution, 0)

    return solution, possible_columns_base

def has_basic_solution(matrix, certificate_items = 0):
    solution, base = find_basic_columns(matrix, certificate_items)
    if len(base) == lines(matrix)-1:
        return True, solution, base

    return False, [], []

def rational_is_int(value):
    temp_value = sp.nsimplify(value, rational=True, tolerance=DEBUG_TOLERANCE)
    denominator = sp.fraction(temp_value)[1]
    debug("rational_is_int? " + str(temp_value) + " " + str(denominator))
    if denominator != 1:
        return False
    return True, denominator

# Given a matrix (that can or cannot have the left matrix) returns
# False if there is a non-integer element in the solution; True otherwise
def has_integer_solution(matrix, A_columns, certificate_items = 0):
    temp_matrix = matrix
    has_solution, full_solution, _ = has_basic_solution(temp_matrix, certificate_items)
    debug("has_integer_solution? - full_solution " + str(full_solution))
    if has_solution:
        int_solution = full_solution[0:A_columns]

        for index, value in enumerate(int_solution):
            if not rational_is_int(value) and value > 0:
                debug("has_integer_solution? - NO " + str(int_solution))
                return False, int_solution

        debug("has_integer_solution? - YES " + str(int_solution))
        return True, int_solution
    return False, []
""" [END] -- Solution functions """

""" [BEGIN] -- Pivot matrix function """
# Given a matrix, line and column for the desired pivot element,
# return a pivoted matrix where pivot is in A[l,c]
def pivot_matrix(matrix, P_line, P_column):
    if P_line is None or P_column is None:
        print("[ERROR] Null index in pivot_matrix function!")
        return False

    # Finds the constant that divides "P_line" in matrix
    # that will turn element matrix[P_line, P_column] into pivot(=1)
    constant = matrix[P_line, P_column]

    # divides the "P_line" in matrix by constant
    matrix[P_line,:] /= constant

    # To each P_line in matrix, except the pivot P_line
    for i in range(0, lines(matrix)):
        if i is not P_line:
            # Find the complement number that makes the element become zero
            complement = -matrix[i,P_column]
            # Adds the pivot line multiplied by the complement
            matrix[i, :] += (matrix[P_line, :]*complement)

    return matrix

# To avoid loops in simplex, when choosing a pivot and there is a draw between two or more values,
# the first value (the one with smaller index) should be chosen
# Otherwise, there is a risk of simplex jumping between bases indefinitely
# Given the pivot_candidates list of tuples (ratio, line, column)
def choose_pivot_with_bland_rule(pivot_candidates):
    if len(pivot_candidates) == 0:
        debug("No PIVOT CANDIDATES")
        return (-1, -1, -1)

    # Will order by ratio first, then line index when there is a draw
    sorted_candidates = sorted(pivot_candidates, key=lambda x: (x[0], x[1]))

    debug_list_of_tuples(sorted_candidates)

    return sorted_candidates[0]

""" [END] -- Pivot matrix function """

""" [BEGIN] -- Shared Print function """
def np_array_to_string(np_array, precision = PRECISION_DECIMALS_CERTIFICATE, format = PRINT_FORMAT_CERTIFICATE):
    if len(np_array) == 0:
        as_str = "[]"
    else:
        # The number of decimal digits and precision is defined in a variable
        # The fractions are printed as float, and 0 is added to eliminate float -0.0
        as_str = np.array2string(
            np_array,
            precision=precision,
            separator=", ",
            formatter={
                'all':lambda x: format % (x+0)
                }
        )
    return as_str

def print_simplex_iteration(output_file, matrix):
    file = open(output_file, "a")
    file.write(np_array_to_string(matrix)+'\n')
    file.close()
""" [END] -- Shared Print function """

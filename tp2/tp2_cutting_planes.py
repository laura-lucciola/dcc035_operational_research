import utils
import sympy as sp
import numpy as np
import tp2_dual
import tp2_bnb_cp_shared as shared

def dual_simplex_for_cutting_planes(matrix, A_columns):
    simplex_result, final_matrix = tp2_dual.solve_by_dual_simplex(matrix)
    if simplex_result is utils.INFEASIBLE:
        return False, final_matrix
    return True, final_matrix

""" Integer programming solver - Cutting Planes """
def cutting_planes_solver(matrix, A_columns, A_lines):
    stop_condition = False

    while not stop_condition:
        # (2) Since the solution is not integer, choose a line where B is fractional and
        # add a new integrality restriction to the matrix
        matrix = create_integer_restriction_matrix(matrix, A_lines)

        # (4.2) solve the new matrix with dual simplex method
        simplex_result, matrix = dual_simplex_for_cutting_planes(matrix, A_columns)
        if simplex_result is False:
            stop_condition = True
            utils.debug("CUTTING PLANES - infeasible")
            return utils.INFEASIBLE, [], -1
        else:
            has_int_solution, solution = utils.has_integer_solution(matrix, A_columns)
            if has_int_solution is True:
                utils.debug("CUTTING PLANES - limited, optimal")
                target_value = utils.get_target_value(matrix)
                return utils.OPTIMAL, solution, target_value

def find_b_candidate(matrix, A_lines):
    b_candidates = []
    for curr_line in range(1, (A_lines+1)):
        utils.debug("line " +str(curr_line))
        # This extra [0] is necessary to unpack the Numpy Array and get the value
        curr_b_item = matrix[curr_line,-1:][0]
        # If the current B element is not a integer
        if not utils.rational_is_int(curr_b_item):
            # Gets the fractionary part from B
            b_fraction_part = curr_b_item - int(curr_b_item)
            b_candidates.append((b_fraction_part, curr_line))
            utils.debug("B candidate -line - b - fraction_part\n" +str(curr_line)+"\t"+str(curr_b_item)+"\t"+str(b_fraction_part))

    # If no Rational B item is found, a stop condition is signalized
    if len(b_candidates) == 0:
        return(-1)

    # Else, find the maximum fractional part in the list (closer to 0.5) and return it's line
    max_fraction = max(b_candidates, key = lambda t: t[0])
    utils.debug("B line - fraction_part\n" +str(max_fraction[1])+"\t"+str(max_fraction[0]))
    return(max_fraction[1])

def create_integer_restriction_matrix(matrix, A_lines):
    b_candidate_line = find_b_candidate(matrix, A_lines)
    if b_candidate_line is not -1:
        mylist = []
        # (3) Get the floor() values of 1st line that has a fractional B and
        # (4.1) add to the matrix as a restriction
        for curr_column in range(0, utils.columns(matrix)):
            floor_num = sp.floor(matrix[b_candidate_line, curr_column])
            temp = sp.nsimplify(floor_num, rational=True, tolerance=utils.DEBUG_TOLERANCE)
            mylist.append(temp)
        source_line = np.array(mylist, dtype=sp.Rational)
        utils.debug("source_line is row - "+ str(b_candidate_line))
        utils.debug_matrix(source_line)
        matrix = shared.add_new_integer_restriction(matrix, source_line)
        return matrix
    return matrix

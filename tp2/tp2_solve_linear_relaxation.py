import numpy as np
import sympy as sp
import utils
# This file contains all functions regarding Auxiliary Simplex method
import tp2_auxiliary
# This file contains all functions regarding Primal Simplex method
import tp2_primal
# This file contains all functions regarding Dual Simplex method
import tp2_dual

""" Given a maximization PL matrix in the following format:
c^T | 0
A   | b
It should be transformed into Standard format (FPI), by adding gap variables:
c^T | 0...0 | 0
A   | I     | b
Then we should try to met the conditions for:
1) Dual simplex, if not possible then for
2) Primal simplex, if not possible then for
3) Auxiliary PL simplex"""
def matrix_to_standart_format(PL_matrix, A_lines, A_columns):
    I_certificate_matrix = utils.create_I_certificate_matrix(A_lines)

    # Split the PL matrix into 2: one with A and C; one with 0 and B
    A_C_matrix = PL_matrix[:,:A_columns]
    b_vector = PL_matrix[:,-1:]

    # Concatenates all matrices to have the standart_format result
    standart_format_matrix = np.concatenate((A_C_matrix, I_certificate_matrix, b_vector), axis=1)
    return standart_format_matrix

def solve_standart_format_matrix(standart_format_matrix, A_lines, A_columns):
    tableau_matrix = utils.create_tableau_matrix_from_standart_format(standart_format_matrix, A_lines)

    utils.debug('tableau_matrix')
    utils.debug_matrix(tableau_matrix)

    # See if the conditions for Dual or Primal simplex are met:
    # The C vector in the tableau (-C from the original PL) has to be >= 0
    # The B vector has to be >= 0
    # A basic solution Cb must exist and be = 0 (guaranteed when adding the gap variables, in this program)
    tableau_C_is_positive = np.all(utils.C_vector_from_tableau(tableau_matrix, A_lines) >= 0)
    B_is_positive = np.all(utils.B_vector(tableau_matrix) >= 0)
    viable_basic_solution = True

    if not B_is_positive:
        if tableau_C_is_positive and viable_basic_solution:
            utils.debug("Solve matrix using DUAL simplex")
            PL_result, final_matrix = tp2_dual.solve_by_dual_simplex(tableau_matrix, A_lines)
            return PL_result, final_matrix
        else:
            # Each element in B vector will become positive
            for curr_line in range(1, utils.lines(tableau_matrix)):
                if tableau_matrix[curr_line,-1:] < 0:
                    tableau_matrix[curr_line,:] *= -1
            B_is_positive = True

            # Check if there is still a Basic solution available
            viable_basic_solution = utils.has_basic_solution(tableau_matrix, A_lines)[0]

    if viable_basic_solution and B_is_positive:
        utils.debug("Solve matrix using PRIMAL simplex")
        PL_result, final_matrix = tp2_primal.solve_by_primal_simplex(tableau_matrix, A_lines)
        return PL_result, final_matrix
    else:
        utils.debug("Solve matrix using AUXILIARY simplex")
        initial_C_line = np.append([], tableau_matrix[0, :])
        PL_result, final_aux_matrix = tp2_auxiliary.auxiliary_simplex_method(tableau_matrix, A_lines)
        # If the auxiliary method results in that the original matrix is feasible
        if PL_result is utils.OPTIMAL:
            basic_solution, _, base = utils.has_basic_solution(final_aux_matrix, A_lines)
            if basic_solution:
                utils.debug("Solve viable matrix using PRIMAL simplex")
                matrix = tp2_auxiliary.get_basic_solution_from_auxiliary(final_aux_matrix, initial_C_line, A_lines)
                tableau_matrix = tp2_auxiliary.solved_aux_matrix_to_canonical_format(matrix, A_lines, base)
                utils.debug("canonical_format_matrix from auxiliary base")
                utils.debug_matrix(tableau_matrix)
                # Using the base found by auxiliary simplex, the primal method is executed
                PL_result, final_matrix = tp2_primal.solve_by_primal_simplex(tableau_matrix, A_lines)
                return PL_result, final_matrix

        return PL_result, final_aux_matrix

import numpy as np
import sympy as sp
import utils

""" Given a matix in tableau and the number of lines in A
(which is the same number of columns of the left matrix in the tableau)
Calculate a line and column among the pivot candidates, for primal or dual simplex"""
def choose_dual_pivot(tableau_matrix, certificate_items = 0):
    # The limit of the A matrix columns is [1st element after the left matrix, if it exists, to one element before B]
    A_first_column = certificate_items
    A_last_column = utils.columns(tableau_matrix)-1
    pivot_candidates = []

    # For each element in B vector (from the second to the last line, of the last column)
    for curr_line in range(1, utils.lines(tableau_matrix)):
        # If the current B element is negative
        if tableau_matrix[curr_line,-1:] < 0:
            # Cycle through all elements in A matrix, in the same line
            for curr_column in range(A_first_column, A_last_column):
                C_item = tableau_matrix[0, curr_column]
                A_item = tableau_matrix[curr_line, curr_column]

                # If a negative A element is in the same column of a positive tableau C item
                # The ratio will be calculated and added to a list with tuples (ratio, B line, A and C column)
                if A_item < 0 and C_item >= 0:
                    curr_ratio = C_item/(-A_item)
                    pivot_candidates.append((curr_ratio, curr_line, curr_column))

    pivot = utils.choose_pivot_with_bland_rule(pivot_candidates)
    return(pivot[1], pivot[2])


def solve_by_dual_simplex(matrix, certificate_items = 0):
    stop_condition = False
    while not stop_condition:
        P_line, P_column = choose_dual_pivot(matrix, certificate_items)

        if P_line == -1:
            stop_condition = True
            break

        matrix = utils.pivot_matrix(matrix, P_line, P_column)
        utils.print_simplex_iteration(utils.ITERATIONS_FILE, matrix)

    B_is_positive = np.all(utils.B_vector(matrix) >= 0)

    utils.debug('Final matrix')
    utils.debug_matrix(matrix)

    # If a stop condition is achieved but B has a negative element, the PL is utils.INFEASIBLE
    if stop_condition and not B_is_positive:
        utils.debug("DUAL simplex - infeasible")
        return utils.INFEASIBLE, matrix

    utils.debug("DUAL simplex - limited, optimal")
    return utils.OPTIMAL, matrix

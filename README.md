# dcc035_operational_research

## Authors/Autores:  
https://gitlab.com/laura-vianna  

### Repository for coursework of Operational Research module - 2018/1  

1. tp1 - simplex method (Primal, Dual, Auxiliary) - Python3:
    * makefile - Contains examples of how the program is executed
    * tp1_main.py - Contains Input reading methods and the main flux for the program
    * utils.py - Contains all functions and variables that are common to all methods
    * tp1_auxiliary.py - Contains all functions regarding Auxiliary Simplex method
    * tp1_primal.py - Contains all functions regarding Primal Simplex method
    * tp1_dual.py- Contains all functions regarding Dual Simplex method


2. tp2 - Integer Programming (Cutting Planes and Branch & Bound) - Python3  
    * makefile - Contains examples of how the program is executed
    * tp2_main.py - Contains Input reading methods and the main flux for the program
    * utils.py - Contains all functions and variables that are common to all methods
    * tp2_auxiliary.py - Auxiliary Simplex method with adaptations
    * tp2_primal.py - Primal Simplex method with adaptations
    * tp2_dual.py- Dual Simplex method with adaptations
    
    * tp2_bnb_cp_shared.py- Contains all functions and variables that are common to Cutting Planes and Branch & Bound
    * tp2_branch_and_bound.py- Contains all functions regarding Branch & Bound method
    * tp2_cutting_planes.py- Contains all functions regarding Cutting Planes method

### Repositorio para trabalho pratico da disciplina Pesquisa Operacional - 2018/1  

1. tp1 - metodo Simplex (Primal, Dual, Auxiliar) - Python3:
    * makefile - Contem exemplos de execucao do programa
    * tp1_main.py - Contem leitura das Entradas e fluxo principal do programa
    * utils.py - Contem funcoes e variaveis comuns a todos os metodos
    * tp1_auxiliary.py - Contem todas as funcoes do metodo Simplex Auxiliar 
    * tp1_primal.py - Contem todas as funcoes do metodo Simplex Primal 
    * tp1_dual.py- Contem todas as funcoes do metodo Simplex Dual 


2. tp2 - Programacao Inteira (Planos de Corete e Branch & Bound) - Python3  
    * makefile - Contem exemplos de execucao do programa
    * tp2_main.py - Contem leitura das Entradas e fluxo principal do programa
    * utils.py - Contem funcoes e variaveis comuns a todos os metodos
    * tp2_auxiliary.py - Simplex Auxiliar com adaptacoes
    * tp2_primal.py - Simplex Primal com adaptacoes
    * tp2_dual.py- Simplex Dual com adaptacoes
    
    * tp2_bnb_cp_shared.py- Contem funcoes e variaveis comuns aos metodos Planos de Corte e Branch & Bound
    * tp2_branch_and_bound.py- Contem todas as funcoes do metodo Branch & Bound
    * tp2_cutting_planes.py- Contem todas as funcoes do metodo Planos de Corte

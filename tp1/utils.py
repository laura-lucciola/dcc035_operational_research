import numpy as np
import sympy as sp

# TODO: set as False for delivery!!!
DEBUG = False

""" Global variables """
INFEASIBLE = 0
UNBOUNDED = 1
OPTIMAL = 2

CONCLUSION_FILE = "conclusao.txt"
ITERATIONS_FILE = "simplex_iterations.txt"

PRINT_FORMAT_CERTIFICATE = '%.6f'
PRECISION_DECIMALS_CERTIFICATE = 6

PRINT_FORMAT_SOLUTION = '%.5f'
PRECISION_DECIMALS_SOLUTION = 5

PRINT_FORMAT_TARGET_VALUE = '%.5f'

DEBUG_TOLERANCE = 0.000001

""" Debug functions """
def debug(arg):
    if DEBUG:
        print("\n\n[DEBUG] "+ str(arg) +" [DEBUG]")

def debug_matrix(matrix):
    if DEBUG:
        # The printing results will wrap the line after 300 characters
        # suppress=True means that no scientific notation is allowed
        # All the elements will be simplified into Rationals (fractions), where the denominator is not greater then 10^6
        np.set_printoptions(
            linewidth=300,
            suppress=True,
            formatter={
                'all':lambda x: str(sp.nsimplify(x, rational=True, tolerance=DEBUG_TOLERANCE))
                }
            )
        print("---[DEBUG_MATRIX]---")
        print(matrix)
        print("---[DEBUG_MATRIX]---\n")

""" Auxiliary print functions """
def np_array_to_string(np_array, precision = PRECISION_DECIMALS_CERTIFICATE, format = PRINT_FORMAT_CERTIFICATE):
    if len(np_array) == 0:
        as_str = "[]"
    else:
        # The number of decimal digits and precision is defined in a variable
        # The fractions are printed as float, and 0 is added to eliminate float -0.0
        as_str = np.array2string(
            np_array,
            precision=precision,
            separator=", ",
            formatter={
                'all':lambda x: format % (x+0)
                }
        )
    return as_str

def print_infeasible(matrix, A_lines):
    certificate = get_certificate_from_tableau(matrix, A_lines)
    print_conclusion_file(CONCLUSION_FILE, INFEASIBLE, certificate)

def print_unbounded(matrix, A_lines, negative_column_index):
    certificate = build_unbounded_certificate(matrix, negative_column_index, A_lines)
    print_conclusion_file(CONCLUSION_FILE, UNBOUNDED, certificate)

def print_optimal(matrix, A_lines, A_columns):
    certificate = get_certificate_from_tableau(matrix, A_lines)
    full_solution = has_basic_solution(matrix, A_lines)[1]
    solution = full_solution[0:A_columns]
    debug("print_optimal function - full_solution")
    debug_matrix(full_solution)
    debug("print_optimal function - solution")
    debug_matrix(solution)
    target_value = get_target_value(matrix)
    print_conclusion_file(CONCLUSION_FILE, OPTIMAL, certificate, solution, target_value)



""" Matrix dimension functions """
def columns(matrix):
    return dimensions(matrix)[1]

def lines(matrix):
    return dimensions(matrix)[0]

# If the matrix has only one column, a Numpy array will throw an exception
def dimensions(matrix):
    try:
        d = (matrix.shape[0], matrix.shape[1])
    except IndexError:
        d = (1, matrix.shape[0])
    return d

""" Matrix creation functions """
# Given a number of lines, creates a matrix in the format:
# 0 0 ... 0
# 1 0 ... 0
# 0 1 ... 0
# 0 0 ... 1
# Which will be named a I_certificate matrix
def create_I_certificate_matrix(lines):
    identity_matrix = np.eye(lines, dtype=sp.Rational)
    certificate_vector = np.zeros((1, lines), dtype=sp.Rational)
    I_certificate_matrix = np.concatenate((certificate_vector, identity_matrix), axis=0)
    return I_certificate_matrix

# Given a matrix in standart_format, creates the tableau matrix:
# 0 ... 0 | -c | 0
# I matrix| A  | B
def create_tableau_matrix_from_standart_format(standart_format_matrix, A_lines):
    # Multiply the C vector by -1 to put in the tableau
    standart_format_matrix[0,:-A_lines-1] *= -1

    # Create a I_certificate to be inserted in the left side of tableau, for registring operations and for the certificate
    left_matrix = create_I_certificate_matrix(A_lines)
    tableau_matrix = np.concatenate((left_matrix, standart_format_matrix), axis=1)
    return tableau_matrix

""" Tableau_Matrix constituents functions """
## certificate     | C from tableau | optimal_value ##
## Identity_matrix | A              | B             ##
def B_vector(matrix):
    return matrix[1:,-1:]

def C_vector_from_tableau(matrix, certificate_items):
    first_c_item = certificate_items
    last_c_item = columns(matrix)-1
    return matrix[0,first_c_item:last_c_item]

def get_target_value(matrix):
    return matrix[0,-1]

def get_certificate_from_tableau(matrix, certificate_items):
    certificate = np.array([], dtype=sp.Rational)
    certificate = matrix[0, 0:certificate_items]
    return certificate


def build_unbounded_certificate(final_matrix, negative_column_index, certificate_items):
    first_A_item = certificate_items
    last_A_item = columns(final_matrix)-1
    certificate = np.array([], dtype=sp.Rational)

    # For each column in the A matrix
    for curr_column in range(first_A_item, last_A_item):
        # If the current column is the one who signaled the UNBOUNDED condition (all elements <=0)
        if curr_column is negative_column_index:
            # The value one will be in the certificate
            certificate = np.append(certificate, 1)
        # Else, verifies if the current column is a basic one
        else:
            is_basic, one_value_line_index = check_if_column_basic(final_matrix, curr_column)
            # If the column is basic
            if is_basic:
                # The complement of the element in the problem column is added to the certificate_items
                # this element is (-1)*matrix[corresponding one value in the basic column index, problem negative column index]
                certificate = np.append(certificate, -1*final_matrix[one_value_line_index, negative_column_index])
            else:
                certificate = np.append(certificate, 0)
    if DEBUG:
        positive_d = np.all(certificate >= 0)
        debug("d >= 0") if positive_d else debug("d < 0")

        A_matrix =  final_matrix[1:, first_A_item:last_A_item]
        Ad = np.dot(A_matrix, certificate)
        Ad_is_zero = np.all(Ad == 0)
        debug("A*d = 0") if Ad_is_zero else debug("A*d != 0")

        minus_C_vector = C_vector_from_tableau(final_matrix, certificate_items) * -1
        Ct_times_d = np.dot(minus_C_vector.T, certificate)
        positive_Ctd = np.all(Ct_times_d > 0)
        debug("c^T*d > 0") if positive_Ctd else debug("c^T*d <= 0")

        debug("Has valid certificate") if (positive_d and Ad_is_zero and positive_Ctd) else debug("Invalid certificate")

    return certificate

""" Basic solution functions """
def check_if_column_basic(matrix, column):
    num_zeros = 0
    num_ones = 0
    one_value_index = -1

    for i in range(0, lines(matrix)):
        if (matrix[i, column] == 0):
            num_zeros += 1
        elif (matrix[i, column] == 1):
            # It is not a Xb if the 1 value corresponds to a negative number in B
            if (matrix[i, -1:] < 0):
                return (False, -1)
            num_ones += 1
            one_value_index = i
            # It is not a Xb if the column has other number other than 0 or 1
        else:
            return (False, -1)

    # It is Xb if the column has exactly one 1 and the rest of it are 0s
    if (num_zeros == lines(matrix)-1) and (num_ones == 1):
        return (True, one_value_index)

    return (False, -1)

def has_basic_solution(matrix, certificate_items):
    solution = np.array([], dtype=sp.Rational)
    base = []
    # Iterates through the columns in the A matrix
    for curr_column in range(certificate_items, columns(matrix) - 1):
        is_basic_column, solution_line_index = check_if_column_basic(matrix, curr_column)

        # If the column is basic, appends the value into the solution array and
        # the column index into the base array
        if is_basic_column:
            base.append(curr_column)
            solution = np.append(solution, matrix[solution_line_index, -1:])
        # Else, the corresponding variable is zero for that column
        else:
            solution = np.append(solution, 0)

    debug("has_basic_solution - solution")
    debug_matrix(solution)
    debug("has_basic_solution - base")
    debug_matrix(base)

    if len(base) == certificate_items:
        return True, solution, base

    return False, [], []

# Given a matrix and a known basic solution base, puts the matrix in canonical form
def to_canonical_format(matrix, A_lines, base):
    for curr_column in base:
        constant = -matrix[0, curr_column]
        for curr_line in range(1, lines(matrix)):
            if matrix[curr_line, curr_column] == 1:
                matrix[0, :] += (matrix[curr_line, :]*constant)
    return matrix

""" (ii) Given a matrix, line and column for the desired pivot element,
return a pivoted matrix where pivot is in A[l,c] """
def pivot_matrix(matrix, P_line, P_column):
    if P_line is None or P_column is None:
        print("[ERROR] Null index in pivot_matrix function!")
        return False

    # Finds the constant that divides "P_line" in matrix
    # that will turn element matrix[P_line, P_column] into pivot(=1)
    constant = matrix[P_line, P_column]

    # divides the "P_line" in matrix by constant
    matrix[P_line,:] /= constant

    # To each P_line in matrix, except the pivot P_line
    for i in range(0, lines(matrix)):
        if i is not P_line:
            # Find the complement number that makes the element become zero
            complement = -matrix[i,P_column]
            # Adds the pivot line multiplied by the complement
            matrix[i, :] += (matrix[P_line, :]*complement)

    return matrix

""" (vi) These methods were built with "file write" unique responsability,
therefore the first method appends a matrix in the same format used in inputs
([[a,b,c],[q,w,e]] with float precision of 3)

The other method writes the conclusion file as required:
0 or 1
[certificate]
for INFEASIBLE and UNBOUNDED PLs and

2
[solution]
target_value
[certificate]
for OPTIMAL PLs
"""
def print_simplex_iteration(input_file, matrix):
    file = open(input_file, "a")
    file.write(np_array_to_string(matrix)+'\n')
    file.close()

def print_conclusion_file(input_file, PL_type, certificate, solution=False, target_value=False):
    debug("print_conclusion_file - "+ str(PL_type))

    if PL_type is OPTIMAL:
        debug("Solution")
        debug_matrix(np_array_to_string(solution, PRECISION_DECIMALS_SOLUTION, PRINT_FORMAT_SOLUTION))
        debug("Target Value")
        debug_matrix(str(PRINT_FORMAT_TARGET_VALUE % target_value)+'\n')

    debug("certificate")
    debug_matrix(np_array_to_string(certificate))

    file = open(input_file, "w")
    certificate_as_str = np_array_to_string(certificate)
    if PL_type is INFEASIBLE or PL_type is UNBOUNDED:
        file.write(str(PL_type)+'\n')
        file.write(certificate_as_str)

    elif PL_type is OPTIMAL:
        file.write(str(PL_type)+'\n')
        file.write(np_array_to_string(solution, PRECISION_DECIMALS_SOLUTION, PRINT_FORMAT_SOLUTION)+'\n')
        file.write(str(PRINT_FORMAT_TARGET_VALUE % target_value)+'\n')
        file.write(certificate_as_str)
    file.close()

import numpy as np
import sympy as sp
import utils

""" (iii) Given a matix in tableau and the number of lines in A
(which is the same number of columns of the left matrix in the tableau)
Calculate a line and column among the pivot candidates, for primal or dual simplex
stop_if_unbounded flag is used when this method is used be auxiliary Simplex method,
since it cannot be UNBOUNDED
"""
def choose_primal_pivot(tableau_matrix, A_lines, stop_if_unbounded = False):
    # The limit of the A matrix columns is [1st element after the left matrix, one element before B]
    A_first_column = A_lines
    A_last_column = utils.columns(tableau_matrix)-1
    pivot_candidates = []

    # For each element in C vector
    for curr_column in range(A_first_column, A_last_column):
        # If the current C element is negative
        if tableau_matrix[0, curr_column] < 0:
            # Cycle through all elements in A matrix, in the same column
            for curr_line in range(1, utils.lines(tableau_matrix)):
                B_item = tableau_matrix[curr_line,-1:]
                A_item = tableau_matrix[curr_line, curr_column]

                # This PL is utils.UNBOUNDED if in a C < 0 column, all A elements are <=0
                if stop_if_unbounded and np.all(tableau_matrix[1:, curr_column] <= 0):
                    return(-1, curr_column, utils.UNBOUNDED)

                # The ratio will be calculated and added to a list with tuples (ratio, B line, A and C column)
                if A_item > 0:
                    curr_ratio = B_item/A_item
                    pivot_candidates.append((curr_ratio, curr_line ,curr_column))

    # If no viable ratio is found
    if len(pivot_candidates) == 0:
        utils.debug("No PIVOT CANDIDATES")
        return(-1, -1, -1)

    # Else, find the minimum ratio in the list and return it's line and column
    min_ratio = min(pivot_candidates, key = lambda t: t[0])
    return(min_ratio[1], min_ratio[2], -1)




def solve_by_primal_simplex(matrix, A_lines, A_columns):
    stop_condition = False

    while not stop_condition:
        positive_C = np.all(utils.C_vector_from_tableau(matrix, A_lines) >= 0)
        positive_B = np.all(utils.B_vector(matrix) >= 0)

        # Verifies if the matrix is already in stop condition for utils.OPTIMAL or utils.INFEASIBLE
        if positive_C:
            utils.debug('Final matrix')
            utils.debug_matrix(matrix)
            if positive_B:
                utils.debug("PRIMAL simplex - limited, optimal")
                utils.print_optimal(matrix, A_lines, A_columns)
                return utils.OPTIMAL
            else:
                utils.debug("PRIMAL simplex - infeasible")
                utils.print_infeasible(matrix, A_lines)
                return utils.INFEASIBLE

        P_line, P_column, result = choose_primal_pivot(matrix, A_lines, stop_if_unbounded=True)

        # Verifies if the matrix is in stop condition for utils.UNBOUNDED
        if P_line is -1:
            stop_condition = True
            if result is utils.UNBOUNDED:
                utils.debug("PRIMAL simplex - unbounded")
                utils.print_unbounded(matrix, A_lines, P_column)
                return utils.UNBOUNDED
            else:
                break

        matrix = utils.pivot_matrix(matrix, P_line, P_column)
        utils.print_simplex_iteration(utils.ITERATIONS_FILE, matrix)

    utils.debug('Final matrix')
    utils.debug_matrix(matrix)

    positive_B = np.all(utils.B_vector(matrix) >= 0)

    # If a stop condition is achieved but B has a negative element, the PL is utils.INFEASIBLE
    if stop_condition and not positive_B:
        utils.debug("PRIMAL simplex - infeasible")
        utils.print_infeasible(matrix, A_lines)
        return utils.INFEASIBLE

    utils.debug("PRIMAL simplex - limited, optimal")
    utils.print_optimal(matrix, A_lines, A_columns)
    return utils.OPTIMAL

"""
Aluna: LAURA VIANNA LUCCIOLA - 2013007544
Disciplina: DCC035 - Pesquisa Operacional
Trabalho Pratico 1
"""

""" (i) Suggested Python library NumPy
Reference: http://cs231n.github.io/python-numpy-tutorial/#numpy
Instead of SciPy's fraction, this project will use the class
Rational() from SymPy library, due it's better compatibility with NumPy Arrays
"""
import numpy as np
import sympy as sp
# Necessary for I/O operatins and error handling
import sys
# This file contains all functions and variables that are common to all methods
import utils
# This file contains all functions regarding Auxiliary Simplex method
import tp1_auxiliary
# This file contains all functions regarding Primal Simplex method
import tp1_primal
# This file contains all functions regarding Dual Simplex method
import tp1_dual


""" (iv) Given a text file name, through the command line, parse the read values into:
A_lines - integer, A_columns - integer, PL_matrix - NumPy Array where each element is a SymPy Rational"""
def read_input_file():
    # Verifies if the input file name was given when calling the program
    if len(sys.argv) < 2:
        print('[ERROR] Wrong number of arguments! Program must call "python3 tp1 <FILENAME>"')
        sys.exit(0)

    filename = sys.argv[1]

    # Treats case when file name is given with or without extension
    if not filename.endswith('.txt'):
        filename += '.txt'

    # Opens the input file and reads values according to the specification
    try:
        input_file = open(filename)
        A_lines = int(input_file.readline())
        A_columns = int(input_file.readline())
        matrix = input_file.readline()
        input_file.close()
    # Treats case when file is not found
    except IOError:
        print('[ERROR] There was an error opening the file! Check if it exists and if the filename is written correctly.')
        sys.exit(0)

    try:
        PL_matrix = np.array(eval(matrix), dtype=sp.Rational)
        return A_lines, A_columns, PL_matrix
    # Treats case when file has a invalid character (that cannot be "eval")
    except SyntaxError:
        print('[ERROR] There was an error while reading the file! Please check if all charcters are valid')
        sys.exit(0)


"""(v) Given a maximization PL matrix in the following format:
c^T | 0
A   | b
It should be transformed into Standard format (FPI), by adding gap variables:
c^T | 0...0 | 0
A   | I     | b
Then we should try to met the conditions for:
1) Dual simplex, if not possible then for
2) Primal simplex, if not possible then for
3) Auxiliary PL simplex"""
def matrix_to_standart_format(PL_matrix, A_lines, A_columns):
    I_certificate_matrix = utils.create_I_certificate_matrix(A_lines)

    # Split the PL matrix into 2: one with A and C; one with 0 and B
    A_C_matrix = PL_matrix[:,:A_columns]
    b_vector = PL_matrix[:,-1:]

    # Concatenates all matrices to have the standart_format result
    standart_format_matrix = np.concatenate((A_C_matrix, I_certificate_matrix, b_vector), axis=1)
    return standart_format_matrix

def solve_standart_format_matrix(standart_format_matrix, A_lines, A_columns):
    tableau_matrix = utils.create_tableau_matrix_from_standart_format(standart_format_matrix, A_lines)

    utils.debug('tableau_matrix')
    utils.debug_matrix(tableau_matrix)

    # See if the conditions for Dual or Primal simplex are met:
    # The C vector in the tableau (-C from the original PL) has to be >= 0
    # The B vector has to be >= 0
    # A basic solution Cb must exist and be = 0 (guaranteed when adding the gap variables, in this program)
    tableau_C_is_positive = np.all(utils.C_vector_from_tableau(tableau_matrix, A_lines) >= 0)
    B_is_positive = np.all(utils.B_vector(tableau_matrix) >= 0)
    viable_basic_solution = True

    if not B_is_positive:
        if tableau_C_is_positive and viable_basic_solution:
            utils.debug("Solve matrix using DUAL simplex")
            tp1_dual.solve_by_dual_simplex(tableau_matrix, A_lines, A_columns)
            return True
        else:
            # Each element in B vector will become positive
            for curr_line in range(1, utils.lines(tableau_matrix)):
                if tableau_matrix[curr_line,-1:] < 0:
                    tableau_matrix[curr_line,:] *= -1
            B_is_positive = True

            # Check if there is still a Basic solution available
            viable_basic_solution = utils.has_basic_solution(tableau_matrix, A_lines)[0]

    if viable_basic_solution and B_is_positive:
        utils.debug("Solve matrix using PRIMAL simplex")
        tp1_primal.solve_by_primal_simplex(tableau_matrix, A_lines, A_columns)
        return True
    else:
        utils.debug("Solve matrix using AUXILIARY simplex")
        initial_C_line = np.append([], tableau_matrix[0, :])
        PL_is_viable, final_aux_matrix = tp1_auxiliary.auxiliary_simplex_method(tableau_matrix, A_lines)
        # If the auxiliary method results in that the original matrix is feasible
        if PL_is_viable:
            basic_solution, _, base = utils.has_basic_solution(final_aux_matrix, A_lines)
            if basic_solution:
                utils.debug("Solve viable matrix using PRIMAL simplex")
                matrix = tp1_auxiliary.get_basic_solution_from_auxiliary(final_aux_matrix, initial_C_line, A_lines)
                tableau_matrix = utils.to_canonical_format(matrix, A_lines, base)
                utils.debug("canonical_format_matrix from auxiliary base")
                utils.debug_matrix(tableau_matrix)
                # Using the base found by auxiliary simplex, the primal method is executed
                tp1_primal.solve_by_primal_simplex(tableau_matrix, A_lines, A_columns)
                return True

        utils.print_infeasible(final_aux_matrix, A_lines)

    return True

""" Main function """
A_lines, A_columns, PL_matrix = read_input_file()
utils.debug('PL_matrix')
utils.debug_matrix(PL_matrix)

SF_matrix = matrix_to_standart_format(PL_matrix, A_lines, A_columns)

solve_standart_format_matrix(SF_matrix, A_lines, A_columns)
